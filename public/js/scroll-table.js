const interColumnGap = 20;
let dataElement,
    columnHeadsElement,
    rowHeadsElement;

function scrollData() {
    columnHeadsElement.scrollLeft = dataElement.scrollLeft;
    rowHeadsElement.scrollTop = dataElement.scrollTop;
}

function init() {
    dataElement = document.getElementById('table-data');
    columnHeadsElement = document.getElementById('table-columnHeads');
    rowHeadsElement = document.getElementById('table-rowHeads');

    // The head of a column defines the column's width
    const columnHeadTds = columnHeadsElement.getElementsByTagName('td');
    const columnsCount = columnHeadTds.length;
    const columnWidths = new Array(columnsCount); // preferred column widths
    for (let i = 0; i < columnsCount; i++)
        columnWidths[i] = columnHeadTds[i].clientWidth + interColumnGap;

    const cellDivs = document.getElementById('table-data').getElementsByTagName('td');
    for (let i = 0; i < columnsCount; i++) {
        columnHeadTds[i].style.minWidth = columnWidths[i] + 'px';
        cellDivs[i].style.minWidth = columnWidths[i] + 'px';
    }
}

// Caution: Makes other keys e.g. F5 not work anymore
function bodyKeydown(e) {
    const element = dataElement;
    const scrollPixels = 128;
    const scrollLeftOld = element.scrollLeft;
     // compliant with ie6
    switch (e.keyCode) {
        case 40: // down
            element.scrollTop += scrollPixels;
            e.preventDefault(); // prevent the default action (scroll / move caret)
            break;
        case 38: // up
            element.scrollTop -= scrollPixels;
            e.preventDefault(); // prevent the default action (scroll / move caret)
            break;
        case 39: // right
            element.scrollLeft += scrollPixels;
            e.preventDefault(); // prevent the default action (scroll / move caret)
            break;
        case 37: // left
            element.scrollLeft -= scrollPixels;
            e.preventDefault(); // prevent the default action (scroll / move caret)
            break;
        case 34: // page down
            element.scrollTop += element.clientHeight;
            e.preventDefault(); // prevent the default action (scroll / move caret)
            break;
        case 33: // page up
            element.scrollTop -= element.clientHeight;
            e.preventDefault(); // prevent the default action (scroll / move caret)
            break;
        case 35: // end
            element.scrollLeft += element.clientWidth;
            if (element.scrollLeft === scrollLeftOld)
                element.scrollTop += element.clientHeight;
            e.preventDefault(); // prevent the default action (scroll / move caret)
            break;
        case 36: // home
            element.scrollLeft -= element.scrollWidth;
            if (element.scrollLeft === scrollLeftOld)
                element.scrollTop -= element.clientHeight;
            e.preventDefault(); // prevent the default action (scroll / move caret)
            break;
    }
}
